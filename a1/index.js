console.log("Hello Alpha");
let trainer = {}
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
         hoenn: ["May", "Max"],
      kanto: ["Brock", "Misty"]
     }
trainer.talk = function(){
    console.log("Pikachu! I choose you!")
}
console.log(trainer);
console.log("Result of dot notaiton");
console.log(trainer.name);
console.log("Result of square notaiton");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
console.log(trainer.talk())

function Pokemon(name,level){
    this.name = name
    this.level = level
    this.health = level * 2;
    this.attack = level * 1.5 ;
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name)
        target.health -= this.attack
        console.log(target.name + " health is now reduce to " + target.health);
        if(target.health <= 0){
         target.faint()   
        }else{
            console.log(target);
        }
    }
    this.faint = function() {
        console.log(this.name + " fainted")
    }
}
let pokemonsDetails = {}
    pokemonsDetails.pikachu = new Pokemon ("Pikachu", 12);
    console.log(pokemonsDetails.pikachu);
    pokemonsDetails.geodude = new Pokemon("Geodude",8);
    console.log(pokemonsDetails.geodude);
    pokemonsDetails.mewtwo = new Pokemon("Mewtwo",100);
    console.log(pokemonsDetails.mewtwo);

console.log(pokemonsDetails.pikachu.tackle(pokemonsDetails.geodude));
console.log(pokemonsDetails.pikachu.tackle(pokemonsDetails.mewtwo));
console.log(pokemonsDetails.mewtwo.tackle(pokemonsDetails.pikachu));